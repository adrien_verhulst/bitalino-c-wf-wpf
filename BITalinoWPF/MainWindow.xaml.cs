﻿// Copyright (c) 2014, Tokyo University of Science All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. * Neither the name of the Tokyo University of Science nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WindowsFormsApplicationBITalino.Utils;

namespace BITalinoWPF
{
    public partial class MainWindow : Window
    {
        static ManagerBitalino manager;

        static int nbFrames;

        static int[] analogChannels;

        static string sConnection = "Connection";

        static string sDeconnection = "Deconnection";

        static string sStartAcquisition = "Start acquisition";

        static string sStopAcquisition = "Stop acquisition";

        Stopwatch timer;

        Thread readThread;

        static bool readContinue;

        List<String> data = new List<String> ( );

        List < BITalinoFrame[] > bitalinoSamples = new List<BITalinoFrame [ ]> ( );

        delegate void SetLogCallback ( BITalinoFrame [ ] f );

        public MainWindow ( )
        {
            manager = new ManagerBitalino ( );
            InitializeComponent ( );
            UpdateCOM ( );
        }

        #region Click events

        private void buttonConnection_Click ( object sender, RoutedEventArgs e )
        {
            try
            {
                if ( manager.IsConnected ( ) == false )
                {
                    MyConnection ( sender, e );
                }
                else
                {
                    MyDeconnection ( sender, e );
                }
            }
            catch ( Exception ex )
            {

                listBoxLog.Items.Insert ( 0, ex.Message );

                if ( manager.IsConnected ( ) == true )
                {
                    MyDeconnection ( sender, e );
                }
            }
        }

        private void buttonStartRead_Click ( object sender, RoutedEventArgs e )
        {
            try
            {
                if ( manager.isAcquisition ( ) == false )
                {
                    MyStart ( );
                }
                else
                {
                    buttonStopRead_Click ( sender, e );
                }
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, ex.Message );
            }
        }

        private void buttonStopRead_Click ( object sender, RoutedEventArgs e )
        {
            try
            {
                MyStop ( );

            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, ex.Message );
            }
        }

        private void buttonClear_Click ( object sender, RoutedEventArgs e )
        {
            listBoxLog.Items.Clear ( );

            data.Clear ( );
        }

        private void labelVersion_Click ( object sender, RoutedEventArgs e )
        {
            UpdateVersion ( );
        }

        private void buttonSaveDataTxt_Click ( object sender, RoutedEventArgs e )
        {
            try
            {
                StreamWriter file = new StreamWriter ( manager.pathTxt );

                foreach ( string sample in data )
                {
                    file.WriteLine ( sample );
                }

                file.Close ( );

                listBoxLog.Items.Insert ( 0, "DONE - sample saved" );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, "Error: " + ex.Message );
            }
        }

        private void buttonSaveDataCsv_Click ( object sender, RoutedEventArgs e )
        {
            try
            {
                StreamWriter file = new StreamWriter ( manager.pathCsv );

                file.WriteLine ( "Time;D1;D2;D3;D4;D5;D6" );

                foreach ( string sample in data )
                {
                    string temp = CSV_Parser.ToCSV ( sample );

                    if ( temp != null )
                    {
                        file.WriteLine ( temp );
                    }
                }

                file.Close ( );

                listBoxLog.Items.Insert ( 0, "DONE - sample saved" );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, "Error: " + ex.Message );
            }
        }

        #endregion

        #region Update

        private void UpdateCOM ( )
        {
            string[] coms = SerialPort.GetPortNames ( );

            foreach ( string com in coms )
            {
                comboxBoxCOM.Items.Add ( com );
            }

            if ( comboxBoxCOM.Items.Count > 0 )
            {
                comboxBoxCOM.SelectedIndex = 0;
            }
        }

        private void UpdateVersion ( )
        {
            version.Content = "Current version:\n " + manager.GetVersion ( );
        }

        private void UpdateCOMName ( )
        {
            manager.PortName = comboxBoxCOM.SelectedItem.ToString ( );
        }

        private void UpdateAnalogChannels ( )
        {
            int position = 0;

            int count = 0;

            for ( int i = 0; i < checkedListBox1.Items.Count; i++ )
            {
                if ( ( ( CheckBox ) checkedListBox1.Items [ i ] ).IsChecked == true )
                {
                    count++;
                }
            }

            analogChannels = new int [ count ];

            for ( int i = 0; i < checkedListBox1.Items.Count; i++ )
            {
                if ( ( ( CheckBox ) checkedListBox1.Items [ i ] ).IsChecked == true )
                {
                    analogChannels [ position ] = i;

                    position++;
                }
            }
        }

        #endregion

        private void MyConnection ( object sender, RoutedEventArgs e )
        {
            listBoxLog.Items.Insert ( 0, " " );

            listBoxLog.Items.Insert ( 0, "Connection " + manager.PortName + "..." );

            manager.Connection ( );

            listBoxLog.Items.Insert ( 0, "DONE - Connection" );

            listBoxLog.Items.Insert ( 0, "BITalino's version" );

            UpdateVersion ( );

            listBoxLog.Items.Insert ( 0, "DONE - BITalino's version" );

            version.Visibility = Visibility.Visible;

            buttonConnection.Content = sDeconnection;
        }

        private void MyDeconnection ( object sender, RoutedEventArgs e )
        {
            listBoxLog.Items.Insert ( 0, "Deconnection" );

            manager.Deconnection ( );

            listBoxLog.Items.Insert ( 0, "DONE - Deconnection" );

            version.Visibility = Visibility.Hidden;

            buttonConnection.Content = sConnection;
        }

        private void MyStart ( )
        {
            UpdateAnalogChannels ( );

            manager.defaultAnalogChannels = analogChannels;

            manager.StartAcquisition ( );

            bitalinoSamples = new List<BITalinoFrame [ ]> ( );

            readContinue = true;

            readThread = new Thread ( Read );

            readThread.Start ( );

            buttonStartRead.Content = sStopAcquisition;
        }

        private void MyStop ( )
        {
            readContinue = false;

            manager.StopAcquisition ( );

            listBoxLog.Items.Insert ( 0, "DONE acquisition stopped" );

            buttonStartRead.Content = sStartAcquisition;
        }

        public void DisplayFrames ( BITalinoFrame [ ] frames )
        {
            this.Dispatcher.Invoke ( DispatcherPriority.Background, new Action (

            delegate ( )
            {
                bitalinoSamples.Add ( frames );

                foreach ( BITalinoFrame frame in frames )
                {
                    listBoxLog.Items.Insert ( 0, "time " + timer.ElapsedMilliseconds + " " + frame.ToString ( ) );

                    data.Insert ( 0, "time " + timer.ElapsedMilliseconds + " " + frame.ToString ( ) );
                }

                if ( listBoxLog.Items.Count > 5000 )
                {
                    for ( int i = listBoxLog.Items.Count; i > 5000; i-- )
                    {
                        listBoxLog.Items.RemoveAt ( i - 1 );
                    }
                }
            } ) );
        }

        public void DisplayError ( string value )
        {
            this.Dispatcher.Invoke ( DispatcherPriority.Background, new Action (

            delegate ( )
            {
                listBoxLog.Items.Insert ( 0, value );

            } ) );
        }

        void Read ( )
        {
            timer = new Stopwatch ( );

            timer.Start ( );

            while ( readContinue == true )
            {
                try
                {
                    BITalinoFrame[] frames = manager.Read ( nbFrames );

                    DisplayFrames ( frames );
                }
                catch ( Exception ex )
                {
                    DisplayError ( "ERROR Read: " + ex.Message );
                }
            }

            timer.Stop ( );
        }

        private void comboBoxFrameRate_SelectedIndexChanged ( object sender, SelectionChangedEventArgs e )
        {
            manager.defaultSamplingRate = int.Parse ( ( ( ComboBoxItem ) comboBoxFrameRate.SelectedItem ).Content.ToString ( ) );
        }

        private void textBoxNbFrames_TextChanged ( object sender, TextChangedEventArgs e )
        {
            try
            {
                nbFrames = Convert.ToInt32 ( textBoxNbFrames.Text );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, ex.Message );
            }
        }

        private void comboxBoxCOM_SelectedIndexChanged ( object sender, SelectionChangedEventArgs e )
        {
            UpdateCOMName ( );
        }

        private void listBoxLog_SelectionChanged ( object sender, SelectionChangedEventArgs e )
        {

        }
    }
}
