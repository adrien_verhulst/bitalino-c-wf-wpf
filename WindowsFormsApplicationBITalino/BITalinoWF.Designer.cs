﻿namespace WindowsFormsApplicationBITalino
{
    partial class BITalinoWF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose ( );
            }
            base.Dispose ( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ( )
        {
            this.buttonSaveDataTxt = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.buttonStartRead = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonSaveDataCsv = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.comboBoxFrameRate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNbFrames = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonConnection = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboxBoxCOM = new System.Windows.Forms.ComboBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.openFDSave = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSaveDataTxt
            // 
            this.buttonSaveDataTxt.Location = new System.Drawing.Point(17, 209);
            this.buttonSaveDataTxt.Name = "buttonSaveDataTxt";
            this.buttonSaveDataTxt.Size = new System.Drawing.Size(160, 24);
            this.buttonSaveDataTxt.TabIndex = 4;
            this.buttonSaveDataTxt.Text = "Save as .txt";
            this.buttonSaveDataTxt.UseVisualStyleBackColor = true;
            this.buttonSaveDataTxt.Click += new System.EventHandler(this.buttonSaveDataTxt_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.HorizontalScrollbar = true;
            this.listBoxLog.Location = new System.Drawing.Point(3, 16);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(304, 281);
            this.listBoxLog.TabIndex = 7;
            // 
            // buttonStartRead
            // 
            this.buttonStartRead.Location = new System.Drawing.Point(18, 179);
            this.buttonStartRead.Name = "buttonStartRead";
            this.buttonStartRead.Size = new System.Drawing.Size(161, 24);
            this.buttonStartRead.TabIndex = 10;
            this.buttonStartRead.Text = "Start acquisition";
            this.buttonStartRead.UseVisualStyleBackColor = true;
            this.buttonStartRead.Click += new System.EventHandler(this.buttonStartRead_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonClear);
            this.groupBox1.Controls.Add(this.buttonSaveDataCsv);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.buttonSaveDataTxt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.buttonStartRead);
            this.groupBox1.Controls.Add(this.checkedListBox1);
            this.groupBox1.Controls.Add(this.comboBoxFrameRate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxNbFrames);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(454, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(184, 300);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acquisition";
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(17, 270);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(160, 24);
            this.buttonClear.TabIndex = 18;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonSaveDataCsv
            // 
            this.buttonSaveDataCsv.Location = new System.Drawing.Point(17, 239);
            this.buttonSaveDataCsv.Name = "buttonSaveDataCsv";
            this.buttonSaveDataCsv.Size = new System.Drawing.Size(160, 24);
            this.buttonSaveDataCsv.TabIndex = 17;
            this.buttonSaveDataCsv.Text = "Save as .csv";
            this.buttonSaveDataCsv.UseVisualStyleBackColor = true;
            this.buttonSaveDataCsv.Click += new System.EventHandler(this.buttonSaveDataCsv_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Data recorded";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Frame rate";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "EMG",
            "EDA",
            "LUX",
            "ECG",
            "ACC",
            "BATT"});
            this.checkedListBox1.Location = new System.Drawing.Point(112, 24);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(67, 79);
            this.checkedListBox1.TabIndex = 14;
            // 
            // comboBoxFrameRate
            // 
            this.comboBoxFrameRate.FormattingEnabled = true;
            this.comboBoxFrameRate.Items.AddRange(new object[] {
            "1000",
            "100",
            "10",
            "1"});
            this.comboBoxFrameRate.Location = new System.Drawing.Point(112, 126);
            this.comboBoxFrameRate.Name = "comboBoxFrameRate";
            this.comboBoxFrameRate.Size = new System.Drawing.Size(66, 21);
            this.comboBoxFrameRate.TabIndex = 13;
            this.comboBoxFrameRate.SelectedIndexChanged += new System.EventHandler(this.comboBoxFrameRate_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Frame(s) by sample";
            // 
            // textBoxNbFrames
            // 
            this.textBoxNbFrames.Location = new System.Drawing.Point(112, 153);
            this.textBoxNbFrames.Name = "textBoxNbFrames";
            this.textBoxNbFrames.Size = new System.Drawing.Size(66, 20);
            this.textBoxNbFrames.TabIndex = 11;
            this.textBoxNbFrames.TextChanged += new System.EventHandler(this.textBoxNbFrames_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonConnection);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comboxBoxCOM);
            this.groupBox2.Controls.Add(this.labelVersion);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(144, 300);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Setup";
            // 
            // buttonConnection
            // 
            this.buttonConnection.Location = new System.Drawing.Point(6, 106);
            this.buttonConnection.Name = "buttonConnection";
            this.buttonConnection.Size = new System.Drawing.Size(84, 26);
            this.buttonConnection.TabIndex = 15;
            this.buttonConnection.Text = "Connection";
            this.buttonConnection.UseVisualStyleBackColor = true;
            this.buttonConnection.Click += new System.EventHandler(this.buttonConnection_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Port\'s BITalino";
            // 
            // comboxBoxCOM
            // 
            this.comboxBoxCOM.FormattingEnabled = true;
            this.comboxBoxCOM.Location = new System.Drawing.Point(6, 37);
            this.comboxBoxCOM.Name = "comboxBoxCOM";
            this.comboxBoxCOM.Size = new System.Drawing.Size(84, 21);
            this.comboxBoxCOM.TabIndex = 13;
            this.comboxBoxCOM.SelectedIndexChanged += new System.EventHandler(this.comboxBoxCOM_SelectedIndexChanged);
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVersion.Location = new System.Drawing.Point(6, 61);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(10, 13);
            this.labelVersion.TabIndex = 13;
            this.labelVersion.Text = "-";
            this.labelVersion.Visible = false;
            this.labelVersion.Click += new System.EventHandler(this.labelVersion_Click);
            // 
            // openFDSave
            // 
            this.openFDSave.FileName = "save.txt";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBoxLog);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(144, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 300);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log(s)";
            // 
            // BITalinoWF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 300);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "BITalinoWF";
            this.Text = "BITalino";
            this.Load += new System.EventHandler(this.BITalinoWF_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSaveDataTxt;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.Button buttonStartRead;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNbFrames;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.ComboBox comboxBoxCOM;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxFrameRate;
        private System.Windows.Forms.Button buttonConnection;
        private System.Windows.Forms.OpenFileDialog openFDSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonSaveDataCsv;
        private System.Windows.Forms.Button buttonClear;
    }
}

