﻿// Copyright (c) 2014, Tokyo University of Science All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. * Neither the name of the Tokyo University of Science nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using WindowsFormsApplicationBITalino.Utils;
using System.Diagnostics;

namespace WindowsFormsApplicationBITalino
{
    public partial class BITalinoWF : Form
    {
        static ManagerBitalino manager;

        static int nbFrames;

        static int[] analogChannels;

        static string sConnection = "Connection";

        static string sDeconnection = "Deconnection";

        static string sStartAcquisition = "Start acquisition";

        static string sStopAcquisition = "Stop acquisition";

        Stopwatch timer;

        Thread readThread;

        static bool readContinue;

        List < BITalinoFrame[] > bitalinoSamples = new List<BITalinoFrame [ ]> ( );

        delegate void SetLogCallback ( BITalinoFrame [ ] f );

        public BITalinoWF ( )
        {
            InitializeComponent ( );
        }

        private void BITalinoWF_Load ( object sender, EventArgs e )
        {
            manager = new ManagerBitalino ( );

            UpdateCOM ( );
        }

        #region Click events

        private void buttonConnection_Click ( object sender, EventArgs e )
        {
            try
            {
                if ( manager.IsConnected ( ) == false )
                {
                    MyConnection ( sender, e );
                }
                else
                {
                    MyDeconnection ( sender, e );
                }
            }
            catch ( Exception ex )
            {

                listBoxLog.Items.Insert ( 0, ex.Message );

                if ( manager.IsConnected ( ) == true )
                {
                    MyDeconnection ( sender, e );
                }
            }
        }

        private void buttonStartRead_Click ( object sender, EventArgs e )
        {
            try
            {
                if ( manager.isAcquisition ( ) == false )
                {
                    MyStart ( );
                }
                else
                {
                    buttonStopRead_Click ( sender, e );

                }
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, ex.Message );
            }
        }

        private void buttonStopRead_Click ( object sender, EventArgs e )
        {
            try
            {
                MyStop ( );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, ex.Message );
            }
        }

        private void buttonClear_Click ( object sender, EventArgs e )
        {
            listBoxLog.Items.Clear ( );
        }

        private void labelVersion_Click ( object sender, EventArgs e )
        {
            UpdateVersion ( );
        }

        private void buttonSaveDataTxt_Click ( object sender, EventArgs e )
        {
            try
            {
                StreamWriter file = new StreamWriter ( manager.pathTxt );

                foreach ( string sample in listBoxLog.Items )
                {
                    file.WriteLine ( sample );
                }

                file.Close ( );

                listBoxLog.Items.Insert ( 0, "DONE - sample saved" );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, "Error: " + ex.Message );
            }
        }

        private void buttonSaveDataCsv_Click ( object sender, EventArgs e )
        {
            try
            {
                StreamWriter file = new StreamWriter ( manager.pathCsv );

                file.WriteLine ( "Time;D1;D2;D3;D4;D5;D6" );

                foreach ( string sample in listBoxLog.Items )
                {
                    string temp = CSV_Parser.ToCSV ( sample );

                    if ( temp != null )
                    {
                        file.WriteLine ( temp );
                    }

                }

                file.Close ( );

                listBoxLog.Items.Insert ( 0, "DONE - sample saved" );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, "Error: " + ex.Message );
            }
        }

        #endregion

        #region Update

        private void UpdateCOM ( )
        {
            comboxBoxCOM.Items.AddRange ( SerialPort.GetPortNames ( ) );

            if ( comboxBoxCOM.Items.Count > 0 )
            {
                comboxBoxCOM.SelectedIndex = 0;
            }
        }

        private void UpdateVersion ( )
        {
            labelVersion.Text = "Current BITalino's version:\n " + manager.GetVersion ( );
        }

        private void UpdateCOMName ( )
        {
            manager.PortName = comboxBoxCOM.SelectedItem.ToString ( );
        }

        private void UpdateAnalogChannels ( )
        {
            analogChannels = new int [ checkedListBox1.CheckedIndices.Count ];

            checkedListBox1.CheckedIndices.CopyTo ( analogChannels, 0 );
        }

        #endregion

        private void MyConnection ( object sender, EventArgs e )
        {
            listBoxLog.Items.Insert ( 0, " " );

            listBoxLog.Items.Insert ( 0, "Connection" + manager.PortName + "..." );

            manager.Connection ( );

            listBoxLog.Items.Insert ( 0, "DONE - Connection" );

            listBoxLog.Items.Insert ( 0, "BITalino's version" );

            UpdateVersion ( );

            listBoxLog.Items.Insert ( 0, "DONE - BItalino's version" );

            labelVersion.Visible = true;

            buttonConnection.Text = sDeconnection;
        }

        private void MyDeconnection ( object sender, EventArgs e )
        {
            listBoxLog.Items.Insert ( 0, "Deconnection" );

            manager.Deconnection ( );

            listBoxLog.Items.Insert ( 0, "DONE - Deconnection" );

            labelVersion.Visible = false;

            buttonConnection.Text = sConnection;
        }

        private void MyStart ( )
        {
            UpdateAnalogChannels ( );

            manager.defaultAnalogChannels = analogChannels;

            manager.StartAcquisition ( );

            bitalinoSamples = new List<BITalinoFrame [ ]> ( );

            readContinue = true;

            readThread = new Thread ( Read );

            readThread.Start ( );

            buttonStartRead.Text = sStopAcquisition;
        }

        private void MyStop ( )
        {
            readContinue = false;

            manager.StopAcquisition ( );

            listBoxLog.Items.Insert ( 0, "DONE - acquisition stopped" );

            buttonStartRead.Text = sStartAcquisition;
        }

        public void DisplayFrames ( BITalinoFrame [ ] frames )
        {
            if ( this.listBoxLog.InvokeRequired )
            {
                SetLogCallback d = new SetLogCallback ( DisplayFrames );

                this.Invoke ( d, new object [ ] { frames } );
            }
            else
            {
                bitalinoSamples.Add ( frames );

                foreach ( BITalinoFrame frame in frames )
                {
                    listBoxLog.Items.Insert ( 0, "time " + timer.ElapsedMilliseconds + " " + frame.ToString ( ) );
                }
            }
        }

        public void DisplayError ( string value )
        {
            if ( InvokeRequired )
            {
                this.Invoke ( new Action<string> ( DisplayError ), new object [ ] { value } );

                return;
            }

            listBoxLog.Items.Insert ( 0, value );
        }

        void Read ( )
        {
            timer = new Stopwatch ( );

            timer.Start ( );

            while ( readContinue == true )
            {
                try
                {
                    BITalinoFrame[] frames = manager.Read ( nbFrames );

                    DisplayFrames ( frames );
                }
                catch ( Exception ex )
                {
                    DisplayError ( "ERROR Read: " + ex.Message );
                }
            }

            timer.Stop ( );
        }

        private void comboxBoxCOM_SelectedIndexChanged ( object sender, EventArgs e )
        {
            UpdateCOMName ( );
        }

        private void comboBoxFrameRate_SelectedIndexChanged ( object sender, EventArgs e )
        {
            manager.defaultSamplingRate = Convert.ToInt32 ( comboBoxFrameRate.SelectedItem.ToString ( ) );
        }

        private void textBoxNbFrames_TextChanged ( object sender, EventArgs e )
        {
            try
            {
                nbFrames = Convert.ToInt32 ( textBoxNbFrames.Text );
            }
            catch ( Exception ex )
            {
                listBoxLog.Items.Insert ( 0, ex.Message );
            }
        }
    }
}
